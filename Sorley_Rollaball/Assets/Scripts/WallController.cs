﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallController : MonoBehaviour
{
    PlayerController pc;//reference to the PlayerController Script
    public int countRequirement = 5; //sets the Count variable requirement for this script
     // Start is called before the first frame update
     private void Start()
     {
        GameObject player = GameObject.FindGameObjectWithTag("Player");//Look for Player object's Game tag
        pc = player.GetComponent<PlayerController>();//Take Player's Script Component
        }

     // Update is called once per frame
      void Update()
        {
        if (pc.count >= countRequirement)//Once COunt is high enough
        {
            Destroy(gameObject);//Destroy this Object

        }

        
         }
}
