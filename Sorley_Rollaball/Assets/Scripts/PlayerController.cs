﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;//Let's player use scene management tools

public class PlayerController : MonoBehaviour
{
    //Gives player a component to reference speed and updating display texts
    public float speed;
    public Text countText;
    public Text winText;
    public float count;
    //Gives script variables to track the rigidbody component of the player and the count of the points currently held
    private Rigidbody rb;
    

    void Start()
    {//sets up variables with variable numbers for text displays and rigidbody component variable
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";
    }

    void FixedUpdate()
    {
        //makes floating variables for inputs to move the ball horizontally or vertically
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        //Uses floating variables to set up a Vector3 variable for the player's position and movement
        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        //Calls the AddForce variable in the player's Rigidbody to move the ball according to the inputs times the players constant speed
        rb.AddForce(movement * speed);
    }

    void OnTriggerEnter(Collider other)
    {//Tells Pick Up object to dissapear and for score counter to be moved up by 1 when player touches the Pick Up
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
        }
    }

    void SetCountText()
    {//Tells Count and Win texts what to display and when to display them
        countText.text = "Count: " + count.ToString();
        if (count >= 10)
        {
            winText.text = "You Win!";
            //invokes a restart after 2 seconds
            Invoke("RestartLevel", 2f);
        }
    }
    void RestartLevel()
    {
        //reload the current level
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);


    }
}
