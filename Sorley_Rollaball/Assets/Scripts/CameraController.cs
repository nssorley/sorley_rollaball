﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    //Makes connection to Player as a game object
    public GameObject player;
    //makes the offset available as a variable within the script
    private Vector3 offset;
    
    // Start is called before the first frame update
    void Start()
    {//Makes the variable, offset, project the cameras location as an offset of the players position
        offset = transform.position - player.transform.position;
    }

    // Update is called once per frame
    void LateUpdate() {
        //Applies the offset variable as an applied transform function on the camera every frame
       transform.position = player.transform.position + offset;
    }
}
